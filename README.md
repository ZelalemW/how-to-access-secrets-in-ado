# How to access SECRETS inside your Custom Visualstudio Marketplace Task built using PowerShell

Note that SECRETS could be stored either in:

- Azure KeyVault or
- Pipeline variable.

There **isn't** any documentation that I could find useful and spend days to figure this thing out on my own - trying so many things and suggessions with no success. I finally solved it on my own and hopefully will help someone else not to waste as much time as I did.

```Powershell

# This gets ALL Task Variables that you can access (including Secret variables)
$allTaskVariablesIncludingSecrets = Get-VstsTaskVariableInfo

# Convert it to json it to see whats available during your debugging - this is just for you to see whats available for you to access.
$allTaskVariablesIncludingSecrets | ConvertTo-Json
#that will give you array of objects with three properties (Name, Secret and Value) in this format:
# [
#     {
#         "Name":  "SecretVariableName",
#         "Secret":  true,
#         "Value":  "***"
#     },
#     {
#         "Name":  "NotSecretVar",
#         "Secret":  false,
#         "Value":  "Some stuff here"
#     }
# ]

# Since our objective is to get a hold of Secret varibales, lets filter them
$secVariables = $allTaskVariablesIncludingSecrets | Where-Object {$_.Secret -eq $true}
# If one of your Secret Variable is called 'SecretVariableName', here is how you access it
$mySecretVarObject = $secVariables |  Where-Object {$_.Name -eq "SecretVariableName"}
$mySecret = $($mySecretVarObject.Value)
# This will give display *** for the value but Length will show you the actual length. So you are good to use $mySecret in your script. You don't NEED to SEE the actual value.
Write-Host "Value: $mySecret and Length: $($mySecret.Length)"

# Simply use $mySecret the way you would any local variable. No special treatment or husle needed

```

Please refer to this doc for more info about the method:
https://github.com/microsoft/azure-pipelines-task-lib/blob/master/powershell/Docs/Commands.md#get-vststaskvariableinfo

**I hope this helps!**

***Happy Coding!***
